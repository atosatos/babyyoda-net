using BooksApi.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;

namespace BooksApi.Services
{
    public class HearingService
    {
        private readonly IMongoCollection<Hearing> _hearings;

        public HearingService(IHearingsDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _hearings = database.GetCollection<Hearing>(settings.HearingsCollectionName);
        }

        public List<Hearing> Get() =>
            _hearings.Find(hearing => true).ToList();

        public Hearing Get(string id) =>
            _hearings.Find<Hearing>(hearing => hearing.Id == id).FirstOrDefault();

        public Hearing Create(Hearing hearing)
        {
            _hearings.InsertOne(hearing);
            return hearing;
        }

        public void Update(string id, Hearing hearingIn) =>
            _hearings.ReplaceOne(hearing => hearing.Id == id, hearingIn);

        public void Remove(Hearing hearingIn) =>
            _hearings.DeleteOne(hearing => hearing.Id == hearingIn.Id);

        public void Remove(string id) => 
            _hearings.DeleteOne(hearing => hearing.Id == id);
    }
}