using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using BooksApi.Models;
using BooksApi.Services;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
namespace BooksApi
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
             services.Configure<BookstoreDatabaseSettings>(
                    Configuration.GetSection(nameof(BookstoreDatabaseSettings)));

            services.Configure<HearingsDatabaseSettings>(
                    Configuration.GetSection(nameof(HearingsDatabaseSettings)));

            services.AddSingleton<IBookstoreDatabaseSettings>(sp =>
                    sp.GetRequiredService<IOptions<BookstoreDatabaseSettings>>().Value);

            services.AddSingleton<IHearingsDatabaseSettings>(sp =>
                    sp.GetRequiredService<IOptions<HearingsDatabaseSettings>>().Value);


            services.AddSingleton<BookService>();

            services.AddSingleton<HearingService>();

            services.AddScoped<BookService>();

            services.AddScoped<HearingService>();

            services.AddCors(options =>
            {
            options.AddPolicy(name: MyAllowSpecificOrigins,
          
                              builder =>
                              {
                                  builder.WithOrigins("*","null");
                                  builder.WithOrigins("http://localhost:4200","null").AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader();
                                  builder.WithOrigins("http://localhost","null").AllowAnyMethod();
                                  builder.WithOrigins("http://localhost:12001","null").AllowAnyMethod();
                                  builder.WithOrigins("http://localhost:5000","null").AllowAnyMethod().AllowAnyOrigin().AllowAnyHeader();
                              });
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();
            app.UseCors(MyAllowSpecificOrigins);

 
            


            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
