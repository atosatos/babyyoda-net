#Get base SDK Image from Microsoft
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 as build-env
WORKDIR /app

# Copy the CSPROJ file and restore any dependecies (bia NUGET)

COPY *.csproj ./
RUN dotnet restore

#Copy the project files and build our eelease
COPY . ./
RUN dotnet publish -c Release -o out

#Generate runtime image

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "BooksApi.dll"]