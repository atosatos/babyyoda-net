using BooksApi.Models;
using BooksApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly BookService _bookService;

        public BooksController(BookService bookService)
        {
            _bookService = bookService;
        }


        [HttpGet]
        [EnableCors("_myAllowSpecificOrigins")]

        public  ActionResult<List<Book>> Get() {
            List<Book> list =_bookService.Get();
            return list;
        }
           

        [HttpGet("hello")]
        [EnableCors("_myAllowSpecificOrigins")]

        public string GetTest(){
            return "Hello";

        }

        [HttpGet("{id:length(24)}", Name = "GetBook")]
        [EnableCors("_myAllowSpecificOrigins")]
        public ActionResult<Book> Get(string id)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            return book;
        }

        [HttpPost]
        [EnableCors("_myAllowSpecificOrigins")]

        public ActionResult<Book> Create(Book book)
        {
            _bookService.Create(book);

            return CreatedAtRoute("GetBook", new { id = book.Id.ToString() }, book);
        }

        [HttpPut("{id:length(24)}")]
        [EnableCors("_myAllowSpecificOrigins")]


        public IActionResult Update(string id, Book bookIn)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Update(id, bookIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        [EnableCors("_myAllowSpecificOrigins")]

        public IActionResult Delete(string id)
        {
            var book = _bookService.Get(id);

            if (book == null)
            {
                return NotFound();
            }

            _bookService.Remove(book.Id);

            return NoContent();
        }
    }
}