using BooksApi.Models;
using BooksApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;

namespace BooksApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HearingsController : ControllerBase
    {
        private readonly HearingService _hearingService;

        public HearingsController(HearingService hearingService)
        {
            _hearingService = hearingService;
        }


        [HttpGet]
        [EnableCors("_myAllowSpecificOrigins")]

        public  ActionResult<List<Hearing>> Get() {
            List<Hearing> list =_hearingService.Get();
            return list;
        }
           

        [HttpGet("hello")]
        [EnableCors("_myAllowSpecificOrigins")]

        public string GetTest(){
            return "Hello";

        }

        [HttpGet("{id:length(24)}", Name = "GetHearing")]
        [EnableCors("_myAllowSpecificOrigins")]
        public ActionResult<Hearing> Get(string id)
        {
            var hearing = _hearingService.Get(id);

            if (hearing == null)
            {
                return NotFound();
            }

            return hearing;
        }

        [HttpPost]
        [EnableCors("_myAllowSpecificOrigins")]

        public ActionResult<Hearing> Create(Hearing hearing)
        {
            _hearingService.Create(hearing);

            return CreatedAtRoute("GetHearing", new { id = hearing.Id.ToString() }, hearing);
        }

        [HttpPut("{id:length(24)}")]
        [EnableCors("_myAllowSpecificOrigins")]


        public IActionResult Update(string id, Hearing hearingIn)
        {
            var hearing = _hearingService.Get(id);

            if (hearing == null)
            {
                return NotFound();
            }

            _hearingService.Update(id, hearingIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        [EnableCors("_myAllowSpecificOrigins")]

        public IActionResult Delete(string id)
        {
            var hearing = _hearingService.Get(id);

            if (hearing == null)
            {
                return NotFound();
            }

            _hearingService.Remove(hearing.Id);

            return NoContent();
        }
    }
}