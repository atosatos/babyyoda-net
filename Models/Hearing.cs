﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace BooksApi.Models
{
    public class Hearing
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("Name")]
        public string CaseNum { get; set; }

        public string HearingType { get; set; }

        public string HearingLocation { get; set; }

        public string Date { get; set; }
    }
}
