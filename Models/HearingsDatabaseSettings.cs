namespace BooksApi.Models
{
    public class HearingsDatabaseSettings : IHearingsDatabaseSettings
    {
        public string HearingsCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IHearingsDatabaseSettings
    {
        string HearingsCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}